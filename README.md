<img src="https://totallynotavir.us/i/u0crxl5k.png" align="right" width="180px"/>

# Variability

[>> Downloads <<](https://gitlab.com/Cynosphere/Variability/tags)

*Randomness, usefulness and cosmetics*

**This mod is open source and under a permissive license.**
This mod can be included in any modpack on any platform without prior
permission. We appreciate hearing about people using our mods, but you do not
need to ask to use them. See the [LICENSE file](LICENSE) for more details.

Variability is yet another random-but-useful stuff mod with focus on cosmetics.

**Notice:** This project uses the [Elytra Project Skeleton](https://github.com/elytra/Skeleton).