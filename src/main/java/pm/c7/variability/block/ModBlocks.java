package pm.c7.variability.block;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.registries.IForgeRegistry;
import pm.c7.variability.Variability;
import pm.c7.variability.block.base.ItemBlockBase;
import pm.c7.variability.config.ConfigHandler;

@GameRegistry.ObjectHolder(Variability.MODID)
public class ModBlocks {
    @GameRegistry.ObjectHolder("crop_flax")
    public static BlockFlaxCrop cropFlax = new BlockFlaxCrop();

    @Mod.EventBusSubscriber(modid = Variability.MODID)
    public static class Registration {
        @SubscribeEvent
        public static void registerBlocks(RegistryEvent.Register<Block> event) {
            IForgeRegistry<Block> registry = event.getRegistry();

            if(ConfigHandler.enableAgriculture) {
                registry.register(cropFlax);
            }
        }

        @SubscribeEvent
        public static void registerItems(RegistryEvent.Register<Item> event) {
            IForgeRegistry<Item> itemRegistry = event.getRegistry();

            if(ConfigHandler.enableAgriculture) {
                itemRegistry.register(new ItemBlockBase(cropFlax));
            }
        }

        @SubscribeEvent
        public static void registerModels (ModelRegistryEvent event) {
            if(ConfigHandler.enableAgriculture) {
                cropFlax.initModel();
            }
        }
    }
}
