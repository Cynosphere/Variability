package pm.c7.variability.events;

import net.minecraftforge.event.entity.player.PlayerWakeUpEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import pm.c7.variability.config.ConfigHandler;
import pm.c7.variability.util.EntityUtil;

public class SleepingBenefits {
    @SubscribeEvent
    public void onPlayerWakeUp(PlayerWakeUpEvent event) {
        if (event.getEntityPlayer() == null || event.getEntityPlayer().world == null || event.getEntityPlayer().world.isRemote) return;

        if (ConfigHandler.sleepRestoreHealth) {
            event.getEntityPlayer().heal((float)ConfigHandler.sleepRestoreHealthAmount);
        }

        if (ConfigHandler.sleepRemoveBadEffects || ConfigHandler.sleepRemoveGoodEffects) {
            EntityUtil.clearEffects(event.getEntityPlayer(), ConfigHandler.sleepRemoveBadEffects, ConfigHandler.sleepRemoveGoodEffects);
        }
    }
}
