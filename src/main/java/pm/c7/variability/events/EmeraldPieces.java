package pm.c7.variability.events;

import net.minecraft.entity.Entity;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.event.entity.living.LivingDropsEvent;
import net.minecraftforge.event.world.BlockEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import pm.c7.variability.config.ConfigHandler;
import pm.c7.variability.item.ModItems;
import pm.c7.variability.util.CustomDataUtil;

public class EmeraldPieces {
    @SubscribeEvent
    public void grassDrops(BlockEvent.HarvestDropsEvent event) {
        if (ConfigHandler.miscEmeraldPieces) {
            if (event.getHarvester() != null && event.getState().getBlock() == Blocks.TALLGRASS) {
                NBTTagCompound data = CustomDataUtil.getPlayerData(event.getHarvester());
                int grassDrops = data.hasKey("emeraldGrassDropsRemaining", 99) ? data.getInteger("emeraldGrassDropsRemaining") : 6;
                if (grassDrops > 0) {
                    int bound = 6 - event.getFortuneLevel();
                    if (bound < 1) bound = 1;

                    int roll = event.getWorld().rand.nextInt(bound);
                    if (roll == 0) {
                        BlockPos pos = event.getPos();
                        EntityItem item = new EntityItem(event.getWorld(), pos.getX() + 0.5, pos.getY() + 0.5, pos.getZ() + 0.5, new ItemStack(ModItems.emeraldChip, 1));
                        event.getWorld().spawnEntity(item);
                        --grassDrops;
                        data.setInteger("emeraldGrassDropsRemaining", grassDrops);
                    }
                }
            }
        }
    }

    @SubscribeEvent
    public void mobDrops(LivingDropsEvent event) {
        if (ConfigHandler.miscEmeraldPieces) {
            if (event.isRecentlyHit()) {
                Entity source = event.getSource().getTrueSource();
                Entity target = event.getEntity();
                if (source instanceof EntityPlayer) {
                    EntityPlayer player = (EntityPlayer) source;
                    World world = player.world;

                    if (!(target instanceof EntityPlayer)) {
                        int xp = player.experienceLevel;
                        int emeralds = 0;

                        if (xp >= 3) {
                            float amount = xp * 0.5F;
                            int guaranteed = (int) (amount * 0.75F);
                            int leftover = (int) (amount + world.rand.nextFloat() - guaranteed);
                            leftover += (int) (amount * event.getLootingLevel() / 2);
                            if (leftover > 0) {
                                leftover = world.rand.nextInt(leftover);
                            }

                            emeralds = guaranteed + leftover;
                        }

                        if (emeralds > 0 && player instanceof EntityPlayerMP) {
                            NBTTagCompound data = CustomDataUtil.getPlayerData(player);
                            BlockPos pos = player.getPosition();

                            int posX = (int)Math.floor(pos.getX());
                            int posZ = (int)Math.floor(pos.getZ());

                            int lastPosX = data.hasKey("emeraldLastX", 99) ? data.getInteger("emeraldLastX") : 0;
                            int lastPosZ = data.hasKey("emeraldLastZ", 99) ? data.getInteger("emeraldLastZ") : 0;

                            int deltaX = lastPosX - posX;
                            int deltaZ = lastPosZ - posZ;

                            double distance = Math.sqrt(deltaX * deltaX + deltaZ + deltaZ);

                            int bonus = (int) Math.floor(distance / 16);

                            int emeraldDrops = data.hasKey("emeraldDropsRemaining", 99) ? data.getInteger("emeraldDropsRemaining") : 6;

                            if (bonus > 0) {
                                data.setInteger("emeraldLastX", posX);
                                data.setInteger("emeraldLastZ", posZ);

                                emeraldDrops += bonus;
                                data.setInteger("emeraldDropsRemaining", emeraldDrops);

                                int grassDrops = data.hasKey("emeraldGrassDropsRemaining", 99) ? data.getInteger("emeraldGrassDropsRemaining") : 6;
                                grassDrops += bonus;
                                if (grassDrops > 6) grassDrops = 6;
                                data.setInteger("emeraldGrassDropsRemaining", grassDrops);
                            }

                            if (emeraldDrops > 0) {
                                ItemStack stack;
                                if (emeralds > 16) {
                                    emeralds /= 16;
                                    if (emeralds > 64) emeralds = 64;

                                    stack = new ItemStack(Items.EMERALD, emeralds);
                                } else if (emeralds > 8) {
                                    emeralds /= 8;
                                    stack = new ItemStack(ModItems.emeraldChunk, emeralds);
                                } else {
                                    stack = new ItemStack(ModItems.emeraldChip, emeralds);
                                }

                                event.getDrops().add(new EntityItem(target.world, target.posX, target.posY, target.posZ, stack));

                                if (emeraldDrops > 6) emeraldDrops = 6;

                                --emeraldDrops;
                                data.setInteger("emeraldDropsRemaining", emeraldDrops);
                            }
                        }
                    }
                }
            }
        }
    }
}
