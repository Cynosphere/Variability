package pm.c7.variability.proxy;

import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;
import pm.c7.variability.Variability;
import pm.c7.variability.events.EmeraldPieces;
import pm.c7.variability.events.SleepingBenefits;
import pm.c7.variability.network.PacketHandler;
import pm.c7.variability.tile.TileEntityBaubleBag;

@Mod.EventBusSubscriber
public class CommonProxy {
    public void preInit(FMLPreInitializationEvent e) {
        PacketHandler.registerMessages();
    }

    public void init(FMLInitializationEvent e) {
        ResourceLocation name = new ResourceLocation(Variability.MODID, "TileEntityBaubleBag");
        GameRegistry.registerTileEntity(TileEntityBaubleBag.class, name);
        MinecraftForge.EVENT_BUS.register(new SleepingBenefits());
        MinecraftForge.EVENT_BUS.register(new EmeraldPieces());
    }

    public void postInit(FMLPostInitializationEvent e) {
    }
}
