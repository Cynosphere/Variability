package pm.c7.variability.util;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;

public class CustomDataUtil {
    public static NBTTagCompound getPlayerData(EntityPlayer player) {
        NBTTagCompound data = player.getEntityData();
        NBTTagCompound variabilityData = data.getCompoundTag("variability");
        if (!data.hasKey("variability", 10)) {
            data.setTag("variability", variabilityData);
        }

        return variabilityData;
    }
}
