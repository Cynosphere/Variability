package pm.c7.variability.util;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;

import java.util.HashSet;
import java.util.Set;

public class EntityUtil {
    public static void clearEffects(EntityPlayer entity, boolean clearBad, boolean clearGood) {
        Set<Potion> toClear = new HashSet<>();

        for (PotionEffect effect : entity.getActivePotionEffects()) {
            boolean isGood = effect.getPotion().isBeneficial();

            if (isGood && clearGood || !isGood && clearBad) {
                toClear.add(effect.getPotion());
            }
        }

        for (Potion effect : toClear) {
            entity.removePotionEffect(effect);
        }
    }
}
