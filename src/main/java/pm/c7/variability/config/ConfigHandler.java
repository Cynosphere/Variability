package pm.c7.variability.config;

public class ConfigHandler {
    //Modules
    public static boolean enableBBag = true;
    public static boolean enableCosmetics = true;
    public static boolean enablePaxels = true;
    public static boolean enableAgriculture = true;

    //Paxels
    public static boolean paxelBCT = false;
    public static boolean paxelIridescent = false;

    //Agriculture
    public static int agrFlaxSeedWeight = 5;

    //Misc
    public static boolean miscEmeraldPieces = true;

    //Sleep
    public static boolean sleepRestoreHealth = true;
    public static double sleepRestoreHealthAmount = 10D;
    public static boolean sleepRemoveBadEffects = true;
    public static boolean sleepRemoveGoodEffects = true;
}
