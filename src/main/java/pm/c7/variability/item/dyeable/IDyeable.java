package pm.c7.variability.item.dyeable;

import net.minecraft.item.ItemStack;

public interface IDyeable {
    //Dummy interface for unified code
    void setColor(ItemStack stack, int color);

    void removeColor(ItemStack stack);

    int getColor(ItemStack stack);

    boolean hasColor(ItemStack stack);
}
